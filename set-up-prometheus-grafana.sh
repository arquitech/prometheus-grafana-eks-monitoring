#!/bin/bash
# add prometheus Helm repo
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts 2>&1 | tee -a prometheus_grafana_install_log.txt

# add grafana Helm repo
helm repo add grafana https://grafana.github.io/helm-charts 2>&1 | tee -a prometheus_grafana_install_log.txt

## Deploy Prometheus ##

#Creates kubernetes namespace to deploy prometheus
kubectl create namespace prometheus 2>&1 | tee -a prometheus_grafana_install_log.txt
#Install prometheus with persistent gp2 storage
helm install prometheus prometheus-community/prometheus --namespace prometheus --set alertmanager.persistentVolume.storageClass="gp2" --set server.persistentVolume.storageClass="gp2" 2>&1 | tee -a prometheus_grafana_install_log.txt

#Wait some time for deploy to complete
echo "Wait 3 min. for prometheus deploy to complete."
sleep 180

#Check Prometheus deploy status
kubectl get all -n prometheus 2>&1 | tee -a prometheus_grafana_install_log.txt

## Set up Grafana ##

#Set up environment file for grafana
curl -Ss https://bitbucket.org/arquitech/prometheus-grafana-eks-monitoring/raw/2f9e52385331a015b750171450c8ec77972723d5/grafana.yaml > ~/grafana.yaml

#Creates kubernetes namespace to deploy grafana
kubectl create namespace grafana 2>&1 | tee -a prometheus_grafana_install_log.txt
#Install grafana with persistent gp2 storage
helm install grafana grafana/grafana --namespace grafana --set persistence.storageClassName="gp2" --set persistence.enabled=true --set adminPassword='EKS!sAWSome' --values /home/ubuntu/grafana.yaml --set service.type=LoadBalancer 2>&1 | tee -a prometheus_grafana_install_log.txt

#Wait some time for deploy to complete
echo "Wait 3 min. for grafana deploy to complete."
sleep 180

#Check grafana deploy status
kubectl get all -n grafana 2>&1 | tee -a prometheus_grafana_install_log.txt

#Wait some time for deploy to complete
echo "Wait some more.."
sleep 300

#Echo the Grafana URL
ELB="$(kubectl get svc -n grafana grafana -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')"
echo "Grafana URL: http://$ELB" 2>&1 | tee -a prometheus_grafana_install_log.txt

#Echo Grafana password and user
ELB_PASSWORD="$(kubectl get secret --namespace grafana grafana -o jsonpath="{.data.admin-password}" | base64 --decode)"
echo "Grafana User: admin, Password: $ELB_PASSWORD" 2>&1 | tee -a prometheus_grafana_install_log.txt
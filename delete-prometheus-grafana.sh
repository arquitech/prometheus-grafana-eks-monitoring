#!/bin/bash
#Delete Prometheus
helm uninstall prometheus --namespace prometheus 2>&1 | tee -a prometheus_grafana_destroy_log.txt
kubectl delete ns prometheus 2>&1 | tee -a prometheus_grafana_destroy_log.txt

#Delete Grafana
helm uninstall grafana --namespace grafana 2>&1 | tee -a prometheus_grafana_destroy_log.txt
kubectl delete ns grafana 2>&1 | tee -a prometheus_grafana_destroy_log.txt
